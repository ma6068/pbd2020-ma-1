package si.uni_lj.fri.pbd.miniapp1.ui.contacts;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.cursoradapter.widget.SimpleCursorAdapter;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.ListFragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;

import java.lang.reflect.Array;
import java.util.AbstractQueue;
import java.util.ArrayList;
import java.util.List;

import si.uni_lj.fri.pbd.miniapp1.MainActivity;
import si.uni_lj.fri.pbd.miniapp1.R;
import si.uni_lj.fri.pbd.miniapp1.ui.message.MessageFragment;

public class ContactsFragment extends Fragment {

    private ArrayList<String> names;
    private ArrayList<String> emails;
    private ArrayList<String> numbers;
    private boolean[] isChecked;

    public void getContacts() {
        names = new ArrayList<>();
        emails = new ArrayList<>();
        numbers = new ArrayList<>();
        // cursor
        ContentResolver contentResolver = getActivity().getContentResolver();
        Cursor cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        // check if there are any contacts
        if (cursor.getCount() > 0) {
            // while we have contacts
            while (cursor.moveToNext()) {
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                // get the name
                names.add(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)));
                // get phone number
                int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)));
                if (hasPhoneNumber > 0) {
                    Cursor cursor2 = contentResolver.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id},
                            null
                    );
                    String number = "NO DATA";
                    while (cursor2.moveToNext()) {
                        number = cursor2.getString(cursor2.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    }
                    numbers.add(number);
                    cursor2.close();
                }
                // get the email
                Cursor cursor3 = contentResolver.query(
                        ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                        null,
                        ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                        new String[]{id}, null);
                String email = "NO DATA";
                while (cursor3.moveToNext()) {
                    // set object's email
                    email = cursor3.getString(cursor3.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS));
                }
                emails.add(email);
                cursor3.close();
            }
            cursor.close();
        }
    }

    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_contacts, container, false);
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            if (names == null) {
                getContacts();
            }
            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                    getContext(),
                    android.R.layout.simple_list_item_multiple_choice,
                    names
            );
            ListView listView = root.findViewById(R.id.list);
            listView.setAdapter(arrayAdapter);
            // we can select more contacts
            listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

            // send data
            Intent intent = new Intent(getActivity(), MessageFragment.class);
            if (emails != null) {
                getActivity().getIntent().putExtra("emails", emails);
                intent.putExtra("emails", emails);
            }
            else {
                emails = getActivity().getIntent().getStringArrayListExtra("emails");
            }
            if (numbers != null) {
                intent.putExtra("numbers", numbers);
                getActivity().getIntent().putExtra("numbers", numbers);
            }
            else {
                numbers = getActivity().getIntent().getStringArrayListExtra("numbers");
            }

            // check if samething is selected before
            if (getActivity().getIntent().getBooleanArrayExtra("isChecked") != null) {
                isChecked = getActivity().getIntent().getBooleanArrayExtra("isChecked");
                // set items checked
                for (int i = 0; i < isChecked.length; i++) {
                    if (isChecked[i]) {
                        listView.setItemChecked(i, true);
                    }
                }
            }
            else {
                isChecked = new boolean[names.size()];
                for (int i=0; i<isChecked.length; i++) {
                    isChecked[i] = false;
                }
            }
            // the user select contacts
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (isChecked[position] == true) {
                        isChecked[position] = false;
                    } else {
                        isChecked[position] = true;
                    }
                    // save the changes;
                    getActivity().getIntent().putExtra("isChecked", isChecked);
                    Intent intent = new Intent(getActivity(), MessageFragment.class);
                    intent.putExtra("isChecked", isChecked);
                }
            });
        }
        else {
            Toast toast = Toast.makeText(getActivity().getApplicationContext(), "You don't have a contacts permission", Toast.LENGTH_LONG);
            toast.show();
        }
        return root;
    }

}


