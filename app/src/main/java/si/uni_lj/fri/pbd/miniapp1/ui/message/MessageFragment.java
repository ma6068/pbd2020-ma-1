package si.uni_lj.fri.pbd.miniapp1.ui.message;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import java.util.ArrayList;

import si.uni_lj.fri.pbd.miniapp1.MainActivity;
import si.uni_lj.fri.pbd.miniapp1.R;
import si.uni_lj.fri.pbd.miniapp1.ui.contacts.ContactsFragment;

import static java.net.Proxy.Type.HTTP;

public class MessageFragment extends Fragment {

    private boolean[] isClicked;
    private ArrayList<String> emails;
    private ArrayList<String> numbers;

    private String errorMessageEmail = "Yout didn't select any contact that have email";
    private String errorMessageMSS = "Yout didn't select any contact that have number";
    private String message = "Sent from my Android mini app 1";

    public void sendMail(int selectedContacts) {
        // put the emails in string table
        String []to = new String[selectedContacts];
        int counter = 0;
        for (int i=0; i<emails.size(); i++) {
            if (isClicked[i] && !emails.get(i).equals("NO DATA")) {
                to[counter] = emails.get(i);
                counter++;
            }
        }
        String subject = "PDB2020 Group mail";
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL, to);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, message);
        try {
            startActivity(Intent.createChooser(intent, "Sending mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getActivity().getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
        }
    }

    public void sendMMS(int selectedContacts) {
        String separator = "; ";
        if(android.os.Build.MANUFACTURER.equalsIgnoreCase("samsung")){
            separator = ", ";
        }
        // get all numbers in one string
        String to = "smsto:";
        boolean firstNumber = true;
        for (int i=0; i<numbers.size(); i++) {
            if (isClicked[i] && !numbers.get(i).equals("NO DATA")) {
                String number = numbers.get(i);
                number = number.replace("(", "").replace(")", "").replace("-", "");
                number = number.replaceAll("\\s","");
                if (firstNumber) {
                    to = to + number;
                    firstNumber = false;
                }
                else {
                    to = to + separator + number;
                }
            }
        }
        try {
            Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("sms", to, null));
            intent.putExtra("sms_body", message);
            startActivity(intent);
        } catch (Exception e) {
            Toast.makeText(getActivity().getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             final ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_message, container, false);

        // get the contacts
        emails = getActivity().getIntent().getStringArrayListExtra("emails");
        numbers = getActivity().getIntent().getStringArrayListExtra("numbers");
        isClicked = getActivity().getIntent().getBooleanArrayExtra("isChecked");

        // get the buttons
        Button emailButton = view.findViewById(R.id.button1);
        Button mmsButton = view.findViewById(R.id.button2);

        // Email
        emailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //check how manu selected contacts are there
                int counter = 0;
                if (isClicked != null && emails != null) {
                    for (int i=0; i<emails.size(); i++) {
                        if (isClicked[i] && emails.get(i) != null && !emails.get(i).equals("NO DATA")) {
                            counter++;
                        }
                    }
                }
                // if there are selected contacts send main
                if (counter > 0) {
                    sendMail(counter);
                }
                // else return an error message
                else {
                    Toast toast = Toast.makeText(getActivity().getApplicationContext(), errorMessageEmail, Toast.LENGTH_LONG);
                    toast.show();
                }
            }
        });

        mmsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) {
                    //check how manu selected contacts are there
                    isClicked = getActivity().getIntent().getBooleanArrayExtra("isChecked");
                    int counter = 0;
                    if (isClicked != null && numbers != null) {
                        for (int i=0; i<numbers.size(); i++) {
                            if (isClicked[i] && numbers.get(i) != null && !numbers.get(i).equals("NO DATA")) {
                                counter++;
                            }
                        }
                    }
                    // if there are selected contacts send mms
                    if (counter > 0) {
                        sendMMS(counter);
                    }
                    // else return an error message
                    else {
                        Toast toast = Toast.makeText(getActivity().getApplicationContext(), errorMessageMSS, Toast.LENGTH_LONG);
                        toast.show();
                    }
                }
                else {
                    Toast toast = Toast.makeText(getActivity().getApplicationContext(), "You don't have a sms permission", Toast.LENGTH_LONG);
                    toast.show();
                }
            }
        });

        return view;
    }
}
